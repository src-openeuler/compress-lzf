Name:                compress-lzf
Version:             1.1.2
Release:             1
Summary:             Basic LZF codec, compatible with standard C LZF package
License:             ASL 2.0
URL:                 https://github.com/ning/compress
Source0:             https://github.com/ning/compress/archive/%{name}-%{version}.tar.gz
BuildRequires:       maven-local mvn(junit:junit) mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:       mvn(org.apache.maven.plugins:maven-enforcer-plugin)
BuildRequires:       mvn(org.apache.maven.surefire:surefire-testng)
BuildRequires:       mvn(org.sonatype.oss:oss-parent:pom:) mvn(org.testng:testng)
BuildRequires:       mvn(com.fasterxml:oss-parent:pom:)
BuildArch:           noarch
%description
Compression codec for LZF encoding for particularly encoding/decoding,
with reasonable compression. Compressor is basic Lempel-Ziv codec,
without Huffman (deflate/gzip) or statistical post-encoding. See
"http://oldhome.schmorp.de/marc/liblzf.html" for more on
original LZF package.

%package javadoc
Summary:             Javadoc for %{name}
%description javadoc
This package contains javadoc for %{name}.

%prep
%autosetup -n compress-%{name}-%{version} -p1
find . -name "*.class" -print -delete
find . -name "*.jar" -type f -print -delete
%pom_remove_plugin org.moditect:moditect-maven-plugin
%pom_xpath_remove "pom:project/pom:build/pom:plugins/pom:plugin[pom:artifactId='maven-javadoc-plugin']/pom:executions"
%pom_add_dep junit:junit::test
%mvn_file : %{name}

%build
%mvn_build -- -Poffline-testing

%install
%mvn_install

%files -f .mfiles
%doc README.md VERSION.txt
%license LICENSE

%files javadoc -f .mfiles-javadoc
%license LICENSE

%changelog
* Fri Jun 28 2024 Ge Wang <wang__ge@126.com> - 1.1.2-1
- Update to 1.1.2

* Thu Dec 14 2023 Ge Wang <wang__ge@126.com> - 1.1-1
- Update to 1.1

* Mon Apr 24 2023 yaoxin <yao_xin001@hoperun.com> - 1.0.4-1
- Update to 1.0.4

* Thu Jul 30 2020 wutao <wutao61@huawei.com> - 1.0.3-1
- package init
